import React from 'react';
import { Switch, Route } from 'react-router-dom';

import LogIn from './pages/LogIn';
import Home from './pages/Home';

const routes = (props) => (
    <Switch>
        <Route exact path="/" component={LogIn} />
        <Route path="/home" component={Home} />
    </Switch>
);

export default routes;
import React, { Component, Fragment } from 'react';
import { Redirect } from 'react-router-dom';

import Context from '../../context/Context';

import logo from '../../assets/images/logo-home.png';

import Input from '../../components/Input';
import Button from '../../components/Button';
import Loader from '../../components/Loader';

import './style.css';

class LogIn extends Component {
    state = {
        email: '',
        password: ''
    }

    handleChange = e => {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {
        return (
            <Context.Consumer>
            {context => (
                <Fragment>
                {context.isLoggedIn &&
                    <Redirect to="/home" />
                }

                {!context.isLoggedIn &&
                    <div className="login-container">
                        <img src={logo} alt="Ioasys logo" />

                        <p className="welcome">BEM-VINDO AO EMPRESAS</p>
                        <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>

                        <div className="log-in">
                            {!context.isLoading &&
                                <Fragment>
                                    {context.hasLoginError &&
                                    <div className="login-error flex-centered-box">
                                        <p>Nome de usuário ou senha inválidos</p>
                                    </div>
                                    }

                                    <Input id="email" type="text" label="E-mail" icon="email" hideLabel onChange={this.handleChange} />
                                    <Input id="password" type="password" label="Senha" icon="cadeado" hideLabel onChange={this.handleChange} />
                                    
                                    <Button value="ENTRAR" onClick={() => context.logIn(this.state)} />
                                </Fragment>
                            }

                            {context.isLoading &&
                                <Loader />
                            }
                        </div>
                    </div>
                }
                </Fragment>
            )}
            </Context.Consumer>
        );
    }
}

export default LogIn;
import React, { Component, Fragment } from 'react';
import { Redirect } from 'react-router-dom';

import Context from '../../context/Context';

import Header from '../../components/Header';

import './style.css';

class Home extends Component {
    render() {
        return (
            <Context.Consumer>
            {context => (
                <Fragment>
                {!context.isLoggedIn &&
                    <Redirect to="/" />
                }

                <Header />
                <div className="home-container flex-centered-box">
                    <h2>Clique na busca para iniciar.</h2>
                </div>
                </Fragment>
            )}
            </Context.Consumer>
        )
    }
}

export default Home;
import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import Provider from './context/Provider';

import Routes from './routes';

function App() {
  return (
    <Provider>
      <div className="App">
        <BrowserRouter>
          <Routes />
        </BrowserRouter>
      </div>
    </Provider>
  );
}

export default App;

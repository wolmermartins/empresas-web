import React from 'react';

import './style.css';

const Loader = (props) => (
    <div className="loader-container">
        <div className="loader-background"></div>
        <div className="loader"></div>
    </div>
);

export default Loader;
import React from 'react';

import './style.css';

const Button = (props) => (
    <div className="button-container flex-centered-box">
        <button type={props.type} onClick={props.onClick}>{props.value}</button>
    </div>
);

export default Button;
import React from 'react';

import Context from '../../context/Context';

import Input from '../Input';

import logo from '../../assets/images/logo-nav.png';
import search from '../../assets/images/ic-search.svg';

import './style.css';

const setSearchButtonClass = (isSearch) => {
    return isSearch ? 'left-button' : 'right-button';
}

const Header = (props) => (
    <Context.Consumer>
    {context => (
        <header>
            <div className="header-container">
            {!context.isSearch &&
                <img className="logo" src={logo} alt="Ioasys logo" />
            }

            <button className={setSearchButtonClass(context.isSearch)} onClick={() => context.search()}>
                <img src={search} alt="Botão pesquisar" />
            </button>

            {context.isSearch &&
                <Input id="search" type="search" label="Pesquisar" hideLabel onChange={context.handleChange} />
            }
            </div>
        </header>
    )}
    </Context.Consumer>
);

export default Header;
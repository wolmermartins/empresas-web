import React from 'react';

import email from '../../assets/images/ic-email.svg';
import lock from '../../assets/images/ic-cadeado.svg';
import search from '../../assets/images/ic-search.svg';

import './style.css';

const setBackgroundImage = (icon) => {
    switch(icon) {
        case 'email':
            return email;
        case 'cadeado':
            return lock;
        case 'search':
            return search;
        default:
            return '';
    }
}

const hideLabel = (props) => {
    if (props.hideLabel) return 'hide-label';
    return '';
}

const Input = (props) => (
    <div className="input-container flex-centered-box">
        <label htmlFor={props.id} className={hideLabel(props)}>{props.label}</label>
        <input id={props.id} type={props.type} name={props.id} value={props.value} placeholder={props.label} onChange={props.onChange} style={{ backgroundImage: `url(${setBackgroundImage(props.icon)})` }} />
    </div>
);

export default Input;
import React, { Component } from 'react';

import Context from './Context';
import api from '../services';

class Provider extends Component {
    state = {
        isLoading: false,
        isLoggedIn: false,
        hasLoginError: false,
        isSearch: false,
        search: ''
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    async search() {
        if (!this.state.isSearch) this.setState({ isSearch: true });
    }

    isAuthenticated() {
        if (sessionStorage.getItem('auth')) this.setState({ isLoggedIn: true });
    }

    async logIn(data) {
        this.setState({ isLoading: true });

        try {
            let user = await api.post('/users/auth/sign_in', data, { 'Content-Type': 'application/json' });
            sessionStorage.setItem('auth', JSON.stringify(user.headers));
            this.setState({ isLoading: false, isLoggedIn: true });
        } catch(err) {
            this.setState({ isLoading: false, hasLoginError: true });
        }
    }

    componentDidMount() {
        this.isAuthenticated();
    }

    render() {
        return (
            <Context.Provider
                value = {{
                    isLoading: this.state.isLoading,
                    isLoggedIn: this.state.isLoggedIn,
                    hasLoginError: this.state.hasLoginError,
                    isSearch: this.state.isSearch,
                    logIn: (data) => this.logIn(data),
                    isAuthenticated: () => this.isAuthenticated(),
                    search: () => this.search(),
                    handleChange: (e) => this.handleChange(e)
                }}
            >
                { this.props.children }
            </Context.Provider>
        );
    }
}

export default Provider;